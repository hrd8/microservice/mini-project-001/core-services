package com.example.serviceregistry;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .disable()
        .authorizeRequests()
        .antMatchers("/eureka/**")
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .httpBasic();
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers(
            "/swagger-ui.html",
            "/resources/**",
            "/static/**",
            "/**/css/**",
            "eureka/js/**",
            "/images/**",
            "/resources/static/**",
            "**/css/**",
            "**/js/**",
            "/img/**",
            "/fonts/**",
            "/images/**",
            "/scss/**",
            "/vendor/**",
            "/favicon.ico",
            "/auth/**",
            "/favicon.png",
            "/v2/api-docs",
            "/configuration/ui",
            "/configuration/security",
            "/webjars/**",
            "/swagger-resources/**",
            "/actuator",
            "/swagger-ui/**",
            "/actuator/**",
            "/swagger-ui/index.html",
            "/swagger-ui/",
            "/v3/api-docs/**",
            "/file/**",
            "/storage/**");
  }
}
