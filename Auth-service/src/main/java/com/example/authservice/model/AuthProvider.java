package com.example.authservice.model;

public enum AuthProvider {
  local,
  facebook,
  google,
  github
}
